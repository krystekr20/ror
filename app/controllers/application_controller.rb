class ApplicationController < ActionController::Base
  http_basic_authenticate_with name: "alena", password: "secret", except: [:index, :show]
  protect_from_forgery with: :exception
  def index
  end
end


